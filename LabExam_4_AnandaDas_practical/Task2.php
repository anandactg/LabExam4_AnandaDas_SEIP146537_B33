<!DOCTYPE html>
<html lang="en">
<head>
    <title>add book</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <div class="panel panel-default">
        <div class="panel-heading"><h2 align="center" >Task 2 File Uploading</h2></div>
            <form action="link.php" method="post"  enctype = "multipart/form-data">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-body">

                        <div class="form-group">
                            <label for="usr">Picture Uploading.....</label>
                            <input type="file" class="form-control-file" id="fileToUpload"  name="fileToUploaded" required>
                        </div>
                        <div class="form-group">

                            <label for="usr">Description of File: </label>
                            <textarea class="form-control" placeholder="Message" rows="6" name="Description" required></textarea>
                        </div>

                        <div class="form-group">
                            <label for="usr">Date:</label><br>
                            <input type="date" placeholder="date" name="date" required>
                        </div>

                        <button type="submit" class="btn btn-default">Send</button>

                        </div>
                </div>
            </form>
    </div>
    <div class="panel-footer">
        <p align="center">Designed by Ananda Das, SEIP 146537, Batch-33</p>
    </div>
</div>

</body>
</html>

